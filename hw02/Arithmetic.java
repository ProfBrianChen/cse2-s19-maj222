///////////////
//// [CSE2] hw02
//// Matthew Jung
//// 2/2/19 
///

// In this assignment, you must compute the cost of the items that you bought
// at the store, including the PA sales tax of 6%.

public class Arithmetic{ 
  
  public static void main (String args[]){
    // Number of pairs of pants
    int numPants = 3; 
    // Cost per pair of pants
    double pantsCost = 34.98;
    // Number of sweatshirts
    int numShirts = 2;
    // Cost per shirt
    double shirtCost = 24.99;
    // Number of belts
    int numBelts = 1;
    // Cost per belt
    double beltCost = 33.99;
    // Sales tax rate in PA (Pennsylvania)
    double paSalesTax = 0.06; 
    // Total cost of all pairs of pants
    double totalCostofPants = numPants * pantsCost; 
    // Total cost of all sweatshirts
    double totalCostofShirts = numShirts * shirtCost;
    // Total cost of belts
    double totalCostofBelts = numBelts * beltCost;
    // Prints sales tax for pants
    double salesTaxofPants = totalCostofPants * paSalesTax;
    // Prints sales tax for sweatshirts 
    double salesTaxofShirts = totalCostofShirts * paSalesTax;
    // Prints sales tax for belts
    double salesTaxofBelts = totalCostofBelts * paSalesTax;
    // Takes sales tax of pants and simplifies answer by lowering number of digits
    double simpleTaxofPants = (int) (100 * salesTaxofPants) / 100.0;
    // Takes sales tax of shirts and simplifies answer by lowering number of digits
    double simpleTaxofShirts = (int) (100 * salesTaxofShirts) / 100.0;
    // Takes sales tax of belts and simplifies answer by lowering number of digits
    double simpleTaxofBelts = (int) (100 * salesTaxofBelts) / 100.0;
    // Total cost of all purchases without sales tax
    double totalCostwithoutTax = totalCostofPants + totalCostofShirts + totalCostofBelts;
    // Total sales tax 
    double totalsalesTax = simpleTaxofPants + simpleTaxofShirts + simpleTaxofBelts;
    // Total cost of all purchases including sales tax
    double totalCostwithTax = totalsalesTax + totalCostwithoutTax;
    // Prints out total cost of pants
    System.out.println("The total cost of the pants will be " + totalCostofPants);  
    // Prints out sales tax of pants
    System.out.println("The sales tax for the pants will be " + simpleTaxofPants);
    // Prints out total cost of sweatshirts
    System.out.println("The total cost of the sweatshirts will be " + totalCostofShirts); 
    // Prints out sales tax of sweatshirts
    System.out.println("The sales tax for the sweatshirts will be " + simpleTaxofShirts);
    // Prints out total cost of belts
    System.out.println("The total cost of the belt will be " + totalCostofBelts);
    // Prints out sales tax of belts
    System.out.println("The sales tax for the belt will be " + simpleTaxofBelts); 
    // Prints out total cost of purchases without sales tax
    System.out.println("The total cost for all purchases WITHOUT tax will be " + totalCostwithoutTax); 
    // Prints out total sales tax
    System.out.println("The total sales tax will be " + totalsalesTax);
    // Prints out total cost of purchases including sales tax
    System.out.println("The total cost for all purchases WITH tax will be "+totalCostwithTax); }

}         
                   
