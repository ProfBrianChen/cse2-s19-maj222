//// [CSE2] lab04
//// Matthew Jung
//// 2/15/19
///

// This program will select a random number from 1 to 52 (Cards 1-13 = diamonds, Cards 14-26 = clubs, Cards 27-39 = hearts, 
// Cards 40 - 52 = spades) to pull out a random card from a deck. 

public class CardGenerator {
  public static void main (String[] args ) {
  // Generates a random # between 1 and 52
  int randomNumber = (int) (Math.random() * 51) + 2;
  // Controls name of suit
  String cardString = "Nothing";
  // Controls type of card
  String typeofCard = "Nothing";
  
  // Determines the name of the suit depending on the random #
    if (randomNumber < 14) {
      cardString = "Diamonds"; 
    }
    else if (randomNumber >= 14 && randomNumber <= 26) {
      cardString = "Clubs";
    }
    else if (randomNumber >= 27 && randomNumber <= 39) {
      cardString = "Hearts";
    }
    else if (randomNumber >= 40 && randomNumber <= 52) {
      cardString = "Spades";
    } 
    
    // Determines the type of card depending on the random #
    // If 1, 14, 27, or 40 are the #, card will be 'Ace"
    switch (randomNumber) {
      case 1: 
        typeofCard = "Ace";
        System.out.println("You picked the " + typeofCard + " of " + cardString);
        break;
      case 14: 
        typeofCard = "Ace";
        System.out.println("You picked the " + typeofCard + " of " + cardString);
        break;
      case 27: 
        typeofCard = "Ace";
        System.out.println("You picked the " + typeofCard + " of " + cardString);
        break;
      case 40: 
        typeofCard = "Ace";
        System.out.println("You picked the " + typeofCard + " of " + cardString);
        break;
     // If 11, 24, 37, and 50 are #, type of card will be 'Jack'
      case 11: 
        typeofCard = "Jack";
        System.out.println("You picked the " + typeofCard + " of " + cardString);
        break;
      case 24: 
        typeofCard = "Jack";
        System.out.println("You picked the " + typeofCard + " of " + cardString);
        break;
      case 37:
        typeofCard = "Jack";
        System.out.println("You picked the " + typeofCard + " of " + cardString);
        break;
      case 50: 
        typeofCard = "Jack";
        System.out.println("You picked the " + typeofCard + " of " + cardString);
        break;
     // If 12, 25, 38, and 51 are #, type of card will be 'Queen'
      case 12: 
        typeofCard = "Queen";
        System.out.println("You picked the " + typeofCard + " of " + cardString);
        break;
      case 25: 
        typeofCard = "Queen";
        System.out.println("You picked the " + typeofCard + " of " + cardString);
        break;
      case 38: 
        typeofCard = "Queen";
        System.out.println("You picked the " + typeofCard + " of " + cardString);
        break;
      case 51: 
        typeofCard = "Queen";
        System.out.println("You picked the " + typeofCard + " of " + cardString);
        break;
     // If 13, 26, 39, and 52 are #, type of card will be King
      case 13: 
        typeofCard = "King";
        System.out.println("You picked the " + typeofCard + " of " + cardString);
        break;
      case 26: 
        typeofCard = "King";
        System.out.println("You picked the " + typeofCard + " of " + cardString);
        break;
      case 39: 
        typeofCard = "King";
        System.out.println("You picked the " + typeofCard + " of " + cardString);
        break;
      case 52: 
        typeofCard = "King";
        System.out.println("You picked the " + typeofCard + " of " + cardString);
        break;
      // If above numbers not chosen, will print out random #
      default: 
        typeofCard = "Nothing";
        System.out.println("You picked the " + randomNumber + " of " + cardString);
        break;
    }
  
}
  
} 
