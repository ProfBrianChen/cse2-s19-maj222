//// [CSE2] hw03
//// Matthew Jung
//// 2/10/19
///

import java.util.Scanner;

// This program can take any measurement in meters and 
// convert it into inches. 

public class Convert {
  // Main method
  public static void main(String[] args) {
  // Tells the scanner to create an instance that will take input from
  // STDIN
  Scanner myScanner = new Scanner (System.in);
  // Asks the user to input a measurement (in meters)
  System.out.print("Enter a distance in meters: ");
  // Accepts the inputted measurement and stores it in STDIN
  double measurement = myScanner.nextDouble();
  // Converts measurement (in meters) to inches
  double meterstoInches = measurement * 39.3701;
  // Simplifies answer by lowering number of digits
  double simplemeterstoInches = (int) (10000.0 * meterstoInches) / 10000.0;
  // Prints out measurement (in meters) converted to inches
  System.out.println(measurement + " meters is " + simplemeterstoInches + " inches. "); }

}
