//// [CSE2] hw03
//// Matthew Jung
//// 2/10/19
///

import java.util.Scanner; 

// This program will prompt the user for the dimensions of a
// box and will print out the volume inside the box. 

public class BoxVolume {
  // Main method
  public static void main(String[] args) {
  // Tells scanner to create an instance that will take input from STDIN
  Scanner myScanner = new Scanner (System.in);
  // Asks the user to input the width of the box
  System.out.print("The width side of the box is: ");
  // Accepts the inputted width and stores it into STDIN
  double widthofBox = myScanner.nextDouble();
  // Asks the user to input the length of the box
  System.out.print("The length of the box is: ");
  // Accepts the inputted length and stores it into STDIN
  double lengthofBox = myScanner.nextDouble();
  // Asks the user to input the height of the box
  System.out.print("The height of the box is: ");
  // Accepts the inputted height and stores it into STDIN
  double heightofBox = myScanner.nextDouble();
  // Calculates volume of the box
  double volumeofBox = widthofBox * lengthofBox * heightofBox;
  // Prints out the volume of the box
  System.out.println("The volume inside the box is: " + volumeofBox); }
  
}
   