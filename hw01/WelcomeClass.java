///////////////
//// [CSE2] hw01 
//// Matthew Jung
//// 1/28/19 
///

public class WelcomeClass{
  
  public static void main(String args[]){
   ////prints ------------- to Terminal window
   System.out.println("  -"+"-"+"-"+"-"+"-"+"-"+"-"+"-"+"-"+"-"+"-"+"-"+"-");
   ////prints | WELCOME | to Terminal window
   System.out.println("  |" + "  WELCOME " + " |");
   ////prints ------------- to Terminal window
   System.out.println("  -"+"-"+"-"+"-"+"-"+"-"+"-"+"-"+"-"+"-"+"-"+"-"+"-");
   ////prints ^ ^ ^ ^ ^ ^ to Terminal window
   System.out.println("  ^ " + " ^ " + " ^ " + " ^ " + " ^ " + " ^ ");
   ////prints / \/ \/ \/ \/ \/ \ to Terminal window
   System.out.println(" /" + " \\/" + " \\/" + " \\/" + " \\/" + " \\/" + " \\");
   ////prints <-M--A--J--2--2--2-> to Terminal window (MAJ222 is my Lehigh network ID)
   System.out.println("<-M"+"--"+"A"+"--"+"J"+"--"+"2"+"--"+"2"+"--"+"2"+"->");
   ////prints \ /\ /\ /\ /\ /\ / to Terminal window
   System.out.println(" \\" + " /\\" + " /\\" + " /\\" + " /\\" + " /\\" + " /"); 
   ////prints v v v v v v to Terminal window
   System.out.println("  v" + "  v" + "  v" + "  v" + "  v" + "  v"); 
   ////prints out First-year college student at Lehigh to Terminal window
   System.out.println("First-year college student at Lehigh."); }
  
}