//// [CSE2] lab03
//// Matthew Jung
//// 2/08/19
///

import java.util.Scanner;

// The purpose of this program is to use the Scanner class to write a program that calculates the 
// original cost of a check, the % tip they wish to pay, and the # of ways the check can be split,
// and then calculate how much each person will have to pay for the check. 

public class Check {
  public static void main(String[] args) {
  // Tells the scanner to create an instance that will take input from STDIN
  Scanner myScanner = new Scanner (System.in);
  // Prints out a request kCost * (1 + tipDecimal);
  // Gives cost of check perto enter what the check originally cost
  System.out.print("Enter the original cost of the check in the form xx.xx: ");
  // Accepts user input from previous command
  // Calls the "nextInt()" method (gets the next random integer value)
  double checkCost = myScanner.nextDouble();
  // Prompts user for tip % they wish to pay
  System.out.print("Enter the percentage tip that you wish to pay as a whole number (in the form xx): ");
  // Converts the tip % into a decimal value
  double tipPercent = myScanner.nextDouble();
  // Creates variable tipDecimal
  double tipDecimal;
  // Converts percentage into decimal value
  tipDecimal = tipPercent / 100;
  // Asks user for the # of people that attended dinner and accepts input
  System.out.print("Enter the number of people who went out to dinner: ");
  int numPeople = myScanner.nextInt();
  double totalCost = checkCost * (1+ tipDecimal);
  // Gets cost of check per person
  double costPerPerson = totalCost / numPeople;
  // Gets the "whole dollar" amount (no decimal values) for each person
  int dollars = (int)costPerPerson;
  // Gets the "dimes" amount and "pennies" ammount (decimal values of cost) for each person
  // e.g. (int) (6.73 * 10) % (NOT A PERCENTAGE) 10 = 67 / 10 = 7
  // % (mod) operator returns remainder after the division
  int dimes = (int) (costPerPerson * 10) % 10;
  int pennies = (int) (costPerPerson * 100) % 10;
  // Prints out how much each person owes
  System.out.println("Each person in the group owes $" + dollars + '.' + dimes + pennies); } 
      
}
